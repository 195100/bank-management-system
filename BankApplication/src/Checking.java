
public class Checking extends Account{
    private static String accountType = "Checking";
    
    Checking(double initialDeposit){
        super();
        this.setBalance(initialDeposit);
        this.checkInterest(0); //in momentul in care se creeaza un cont dobanda e zero, iar in functie de suma depusa, daca trece de 10000, atunci ea va lua valoarea 0.05 sau 0.02;
    }
    
    @Override
    public String toString(){
        return "Account type: " + accountType + " Account" + "\n" + 
               "AccountNumber " + this.getAccountNumber() + "\n" + 
               "Balance $" + this.getBalance() + "\n" + 
               "Interest rate: " + this.getInterest() + "%\n";
    }
}
