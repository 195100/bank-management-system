
import java.util.ArrayList;

public class Bank {
    ArrayList<Customer> customers = new ArrayList<Customer>();
    
    void addCustomer(Customer customer) {
        customers.add(customer);
    }

    //getter pt customer
    Customer getCustomer(int account) {
        return customers.get(account);
    }
    
    //returneaza lista intreaga
    ArrayList<Customer> getCustomers(){
        return customers;
    }
}
