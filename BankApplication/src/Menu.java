
import java.util.ArrayList;
import java.util.Scanner;


public class Menu {
    //creez instantele
    Scanner keyboard = new Scanner(System.in);
    Bank bank = new Bank();
    boolean exit;
    
    public static void main(String[] args){
        Menu menu = new Menu();
        menu.runMenu();
    }
    
    public void runMenu(){
        printHeader();
        while(!exit){ //daca nu pun acest while, pot executa o singura data o operatie;
            printMenu();
            int choice = getInput();
            performAction(choice);
        }
    }

    private void printHeader() {
        System.out.println("+----------------------------------------+");
        System.out.println("!           Welcome to our bank!         !");
        System.out.println("!                 Enjoy!                 !");
        System.out.println("+----------------------------------------+");
    }

    private void printMenu() {
        System.out.println("Please make a selection: ");
        System.out.println("1) Create a new account");
        System.out.println("2) Make a deposit");
        System.out.println("3) Make a withdrowal");
        System.out.println("4) List account balance");
        System.out.println("0) Exit");
    }

    private int getInput(){
        int choice = -1;
        do{
            System.out.print("Enter your choice: ");
            try{
                choice = Integer.parseInt(keyboard.nextLine());
            }catch(NumberFormatException e){
                System.out.println("Invalid selection. Numbers only please!");
            }
            if(choice < 0 || choice > 4){
                System.out.println("Choice out of range, please try again!");
            }
        }while(choice < 0 || choice > 4);
        return choice;
    }

    private void performAction(int choice) {
        switch(choice){
            case 0:
                System.out.print("Thank you for using our application");
                System.exit(0);
                break;
            case 1:
                createAnAccount();
                break;
            case 2:
                makeADeposit();
                break;
            case 3:
                makeAWithdrawal();
                break;
            case 4:
                listBalances();
                break;
            default:
                System.out.println("Unknown error has occurred");
        }
    }

    private void createAnAccount() {
        String userNames, firstName, lastName, ssn, accountType = "";
        double initialDeposit = 0;
        boolean valid = false;
        //aici verificam daca tipul de cont este corect
        while(!valid){
            System.out.println("Please enter an account type(checking/savings): ");
            accountType = keyboard.nextLine();
            if(accountType.equalsIgnoreCase("checking") || accountType.equalsIgnoreCase("savings")){
                valid = true;
            }else{
                System.out.println("Wrong account type. Please enter checking or savings!");
            }
        }
        
        //aici luam datele utilizatorului
        System.out.print("Please enter your first name: ");
        firstName = keyboard.nextLine();
            if(!firstName.matches("[a-zA-Z]+")){
                System.out.println("Wrong first name. Try again.");
                return;
            }
            
        System.out.print("Please enter your last name: ");
        lastName = keyboard.nextLine();
            if(!lastName.matches("[a-zA-Z]+")){
                System.out.println("Wrong last name. Try again.");
                return;
            }
            
        System.out.print("Please enter your social security number: ");
        ssn = keyboard.nextLine();
            if(!ssn.matches("[0-9]+") || (String.valueOf(ssn).length()) != 10){
                System.out.println("SSN must be a number and consists of 10 digits.");
                return;
            } 
        
        //aici verificam daca contul are minumul de depozit cerut la creare
        valid = false;
        while(!valid){
            System.out.println("Please enter an initial deposit: ");
            try{
                initialDeposit = Double.parseDouble(keyboard.nextLine());
            } catch(NumberFormatException e){
                System.out.print("Deposit must be a number.");
            }
            
            if(accountType.equalsIgnoreCase("checking")){
                if(initialDeposit < 100){
                    System.out.print("Checking accounts require a minimum of $100 dolars to open");
                }else{
                    valid = true;
                }
            }else if(accountType.equalsIgnoreCase("savings")){
                if(initialDeposit < 50){
                    System.out.print(" Savings accounts require a minimum of $50 dolars to open");
                }else{
                    valid = true;
                }
            }
        }
        
        // crearea contului;
        Account account; //am creat un obiect din clasa account
        if(accountType.equalsIgnoreCase("checking")){
            account = new Checking(initialDeposit);
            System.out.println("Congratulations. Your account has been created.");
        }else{
            account = new Savings(initialDeposit);
            System.out.println("Congratulations. Your account has been created.");
        } 
        
        //crearea unui utilizator = customer;
        Customer customer = new Customer(firstName, lastName, ssn, account);
        
        //adaugarea acestui customer la banca
        bank.addCustomer(customer);
    }

    private void makeADeposit() {
        int account = selectAccount();
        if(account >= 0){
            System.out.print("How much would you like to deposit? ");
            double amount = 0;
            try{
                amount = Double.parseDouble(keyboard.nextLine());
            }catch(NumberFormatException e){
                amount = 0;
            }
            //avem banca, luam customer-ul, luam account-ul lui si facem un deposit in cont
            bank.getCustomer(account).getAccount().deposit(amount); //suntem in bank, mergem in customer, de unde se face return account-ului, unde facem deposit;
        }
    }

    private void makeAWithdrawal() {
        int account = selectAccount();
        if(account >= 0){
            System.out.print("How much would you like to withdraw? ");
            double amount = 0;
            try{
                amount = Double.parseDouble(keyboard.nextLine());
            }catch(NumberFormatException e){
                amount = 0;
            }
            //avem banca, luam customer-ul, luam account-ul lui si facem un deposit in cont
            bank.getCustomer(account).getAccount().withdraw(amount);
        }
    }

    private void listBalances() {
        int account = selectAccount();
        if(account >= 0){
            System.out.println(bank.getCustomer(account).getAccount());
        }
    }

    private int selectAccount() {
        ArrayList<Customer> customers = bank.getCustomers();
        if(customers.size() <= 0){
            System.out.print("No customers at your bank;");
            return -1;
        }
        System.out.println("Select an account.");
        for(int i = 0; i < customers.size(); i++){
            System.out.println((i+1) + ")" + customers.get(i).basicInfo());
        }
        int account = 0;
        System.out.print("Please enter your selection(account)...");
        try{
            account = Integer.parseInt(keyboard.nextLine()) - 1;
        }catch(NumberFormatException e){
            account = -1;
        }
        
        //verifica daca accountul este valid
        if(account < 0 || account > customers.size()){
            System.out.print("Invalid account selected");
            account = - 1;
        }
        return account;
    }
}
