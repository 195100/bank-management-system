
public class Customer {
    private final String firstName;
    private final String lastName;
    private final String ssn;
    private final Account account;

    Customer(String firstName, String lastName, String ssn, Account account) {
       this.firstName = firstName;
       this.lastName = lastName;
       this.ssn = ssn;
       this.account = account;
    }
    
//    @Override
//    public String toString(){
//        return "\nCustomer Information\n" + 
//               " First Name: " + firstName + "\n" + 
//               " Last Name: " + lastName + "\n" +
//               " SSN: " + ssn + "\n" +
//                account; // va fi apelata metoda toString() din account(savings sau checking);
//    }
    
     public String basicInfo(){
        return "First Name: " + firstName + "," +
               " Last Name: " + lastName + "," +
               " SSN: " + ssn + "," +
               " Account Number: " + account.getAccountNumber(); // va fi apelata metoda toString() din account(savings sau checking);
    }
     
    //getter pentru account //asa leg customer de account;
    Account getAccount(){
        return account;
    }
}
