
public class Savings extends Account{
    private static String accountType = "Savings";
    
     Savings(double initialDeposit){
        super();
        this.setBalance(initialDeposit);
        this.checkInterest(0);
    }
    
    @Override
    public String toString(){
        return "Account type: " + accountType + " Account" + "\n" + 
               "AccountNumber " + this.getAccountNumber() + "\n" + 
               "Balance $" + this.getBalance() + "\n" + 
               "Interest rate: " + this.getInterest() + "%\n";
    }
}

